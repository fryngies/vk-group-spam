from PyQt5 import QtCore, QtWebKitWidgets, QtWidgets

import sys


class Browser_Ui(object):
	def setupUi(self, Form):
		Form.setObjectName("VK Login")
		Form.resize(480, 640)
		self.verticalLayout = QtWidgets.QVBoxLayout(Form)
		self.verticalLayout.setContentsMargins(0, 0, 0, 0)
		self.verticalLayout.setObjectName("verticalLayout")
		self.webView = QtWebKitWidgets.QWebView(Form)
		self.webView.setUrl(QtCore.QUrl("about:blank"))
		self.webView.setObjectName("webView")
		self.verticalLayout.addWidget(self.webView)

class Browser(QtWidgets.QWidget, Browser_Ui):
	def __init__(self, parent=None):
		super(Browser, self).__init__(parent)

		self.setupUi(self)

	def setUrl(self, url):
		self.webView.setUrl(url)


if __name__ == '__main__':
	app = QtWidgets.QApplication(sys.argv)

	win = Browser()
	win.setUrl(QtCore.QUrl('http://m.vk.com'))
	win.show()

	sys.exit(app.exec_())
