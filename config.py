# -*- coding: utf-8 -*-
import configparser, codecs

class config:
	def __init__(self, filepath):
		self.config = configparser.ConfigParser()
		self.config.readfp(codecs.open(filepath, "r", "utf8"))

	def get_data(self, name):
		return self.config['MAIN'][name]
