# -*- coding: utf-8 -*-
import json

class json_db:
	def __init__(self, f):
		self.db = json.loads(f)


class spammers(json_db):
	def add_user():
		pass

	def remove_user():
		pass

	def edit_user():
		pass

	def get_spammer_data(self, user_id):
		return self.db['spammers'][user_id]

	def get_spammers_count(self):
		return len(self.db['spammers'])


class victims(json_db):
	def add_victims(self, victims):
		pass

	def mark_victim(self, victim_id):
		pass

	def get_victims_count(self):
		return len(self.db['victims'])
