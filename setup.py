# -*- coding: utf-8 -*-
from distutils.core import setup
import py2exe
import requests.certs

setup(console=['spam.py'],
	data_files=[('', [requests.certs.where()])])
