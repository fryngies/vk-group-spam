# -*- coding: utf-8 -*-
import os, logging, random, json, threading, atexit, webbrowser
import db, config, spammer
from time import sleep


## Set paths
# Workdir
root = os.getcwd()

# Fix for Windows (py2exe)
os.environ["REQUESTS_CA_BUNDLE"] = os.path.join(root, "cacert.pem")

# Path to config
cfg = config.config(os.path.join(root, 'config.ini'))

# Logging configuration
logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()

fileHandler = logging.FileHandler("{0}/{1}.log".format(root, 'spam'))
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)
rootLogger.setLevel(logging.INFO)

## Read our spam messages
f = open('message.txt', 'r', encoding='utf8')
msg = f.read()
f.close()

f = open('request_message.txt', 'r', encoding='utf8')
req_msg = f.read()
f.close()

lock = threading.Lock()


def get_db(filepath, group_id, spammer):
	if not os.path.isfile(filepath):
		logging.info("База данных пользователей не существует, извлечение...")
		victims = spammer.get_all_group_members(group_id)
		with open(filepath, 'w', encoding='utf8') as f:
			f.write(json.dumps(victims, ensure_ascii=False))
		return victims
	else:
		logging.info("Используется существующая база данных пользователей.")
		with open(filepath, 'r', encoding='utf8') as f:
			victims = f.read()
			return json.loads(victims)

def write_db(spammers, db_name):
	with open(db_name, 'w', encoding='utf8') as f:
		f.write(json.dumps(spammers, ensure_ascii=False))

def get_spammers(timeout, sleep_time, db_spammers):
	spammers = []
	for i in range(db_spammers.get_spammers_count()):
		spammers.append(spammer.spammer(db_spammers.get_spammer_data(i)['access_token'], timeout, sleep_time))
	return spammers

def get_users(spammer, victims):
	lock.acquire()

	result = []

	if victims:
		result = victims[0]
		if result['can_write_private_message']:
			del victims[0]
		elif victims[0]['spam_status']:
			friendship = spammer.make_friendship(result, req_msg)
			if friendship == 2:
				logging.info("Запрос для %s %s одобрен" % 
				 (result['last_name'], result['first_name']))
				result['spam_status'] = 0
				del victims[0]
		else:
			friendship = spammer.make_friendship(result, req_msg)

			if friendship != 175:
				logging.info("Отправка запроса на добавление в друзья пользователю %s %s" % 
				 (result['last_name'], result['first_name']))

				victims[0]['spam_status'] = 1
			elif friendship == 175:
				logging.info("Пользователь %s %s добавил в игнор, удаление." % 
				 (result['last_name'], result['first_name']))

				del victims[0]
				victims.append(result)

		sleep(float(cfg.get_data('Sleep between requests')))

	lock.release()

	return result

def add_spammers(num, db_name, app_id):
	spammers = {"spammers": []}

	for i in range(0, num):
		webbrowser.open("http://oauth.vk.com/authorize?client_id=%s&scope=friends,messages,groups,offline&redirect_uri=https://oauth.vk.com/blank.html&display=popup&v=5.27&response_type=token" % 
			(app_id))

	for i in range(0, num):
		spammers['spammers'].append({"access_token": input("Access token для спаммера #%i: " % (i+1))})

	write_db(spammers, db_name)

	return 0

def spam(spammer, victims):
	while True:
		victim = get_users(spammer, victims)

		if not victim:
			logging.info("Список пуст.")
			break

		if not victim['spam_status']:
			logging.info("Отправка сообщения пользователю %s %s" %
				(victim['last_name'], victim['first_name']))
			spammer.send_msg(message=msg+'\n'+str(random.randint(1, 100)), user_id=victim['id'])
		else:
			sleep(float(cfg.get_data('Sleep between requests')) * 100)

		sleep(float(cfg.get_data('Sleep between requests')))

def main():
	group_id = cfg.get_data('ID of the attacked group')
	users_db_path = os.path.join(root, str(group_id) + '-users.json')
	# Path to users database
	db_path = os.path.join(root, cfg.get_data('Users database name'))

	if not os.path.isfile(cfg.get_data('Users database name')):
		add_spammers(int(cfg.get_data('Number of bots')), cfg.get_data('Users database name'), cfg.get_data('App ID'))

	## Init database
	# Open database file
	f = open(os.path.join(root, db_path))
	db_file = f.read()
	f.close()
	# Init database
	db_spammers = db.spammers(db_file)
	# Make sure that we will not use this var further
	del db_file

	# Initialize spammers
	spammers = get_spammers(float(cfg.get_data('Request timeout')), float(cfg.get_data('Sleep between requests')), db_spammers)

	# Initialize database
	# TODO: rewrite with multithreading support (is it really needed?)
	victims = get_db(users_db_path, group_id, spammers[0])

	atexit.register(write_db, victims, users_db_path)

	# Initialize threads
	threads = []
	for i in range(0, db_spammers.get_spammers_count()):
		threads.append(threading.Thread(target=spam, args=(spammers[i], victims)))
	for i in range(0, db_spammers.get_spammers_count()):
		threads[i].start()
	for i in range(0, db_spammers.get_spammers_count()):
		threads[i].join()

if __name__ == '__main__':
	main()
