# -*- coding: utf-8 -*-
import sys, logging, math
import vk
from time import sleep

class spammer:
	def __init__(self, spammer_access_token, timeout, sleep_time):
		self.vkapi = vk.API(access_token=spammer_access_token, timeout=timeout)
		self.sleep_time = sleep_time

	def send_msg(self, message, domain=None, user_id=None):
		return self.vkapi.messages.send(user_id=user_id, domain=domain, message=message)

	def get_all_group_members(self, group_id, offset=0):
		req_count = 1000

		list = self.get_group_members(group_id=group_id,offset=0,count=0,fields='can_write_private_message')
		total_count = list['count']
		num = math.ceil(total_count / 1000)

		sleep(self.sleep_time)

		logging.info("Жертв: %s, запросов: %s" % (total_count, num))

		for i in range(0, num):
			while True:
				try:
					list['items'] += self.get_group_members(group_id=group_id,offset=offset,count=req_count,fields='can_write_private_message')['items']
				except:
					print("\n!!! Ошибка:", sys.exc_info()[0])
				sleep(self.sleep_time)
				break
			offset += 1000
			sys.stdout.write('\rИзвлечено: %.2f%%' % round((100 - (num-(i+1)) / num * 100), 2))
			sys.stdout.flush()

		sys.stdout.write('\n')

		for i in list['items']:
			i['spam_status'] = 0

		return list['items']

	def get_wall_data(self):
		return self.vkapi.wall.get()

	def get_group_members(self, count, offset, group_id, filt='', fields=''):
		return self.vkapi.groups.getMembers(count=count, offset=offset, group_id=group_id, filter=filt, fields=fields)

	def make_friendship(self, victim, msg):
		status = self.vkapi.friends.add(user_id=victim['id'], text=msg)

		return status
